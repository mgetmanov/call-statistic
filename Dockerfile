FROM apacheignite/ignite:2.0.0

COPY ignite/build/libs/ignite*.jar $IGNITE_HOME/libs/provider/
COPY commons/build/libs/commons*.jar $IGNITE_HOME/libs/provider/
