package name.gem.provider.commons.cfg;

import java.util.Properties;

public class ConfigApp {
    public static final String IS_SERVER = "server";
    public static final String IS_GENERATOR = "generator";
    public static final String CALL_PER_SECOND = "cal_per_second";
    public static final String EVENT_OF_COST = "event_of_cost";

    private ConfigApp() {

    }

    public static boolean isServer() {
        return Boolean.parseBoolean(System.getProperty(IS_SERVER));
    }

    public static boolean isGenerationMode() {
        return Boolean.parseBoolean(System.getProperty(IS_GENERATOR));
    }

    public static int callPerSeconds() {
        try {
            return Math.abs(Integer.parseInt(System.getProperty(CALL_PER_SECOND)));
        } catch (Exception e) {
            return 100;
        }
    }

    public static int eventOfCost() {
        try {
            return Math.abs(Integer.parseInt(System.getProperty(EVENT_OF_COST)));
        } catch (Exception e) {
            return 100;
        }
    }

    public static Properties getPropertiesForKafka() {
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        return props;
    }
}
