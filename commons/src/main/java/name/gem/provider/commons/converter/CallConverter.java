package name.gem.provider.commons.converter;

import name.gem.provider.commons.model.Call;
import name.gem.provider.commons.model.CallStatistic;
import name.gem.provider.commons.utils.DateUtils;
import org.apache.ignite.IgniteBinary;
import org.apache.ignite.binary.BinaryObject;

public class CallConverter {
    private final IgniteBinary binary;

    private CallConverter(IgniteBinary binary) {
        this.binary = binary;
    }


    public static CallConverter build(IgniteBinary binary) {
        return new CallConverter(binary);
    }

    public BinaryObject binaryObject(Call call) {
        return binary.toBinary(call);
    }

    public Call source(Object object) {
        if (!(object instanceof Call)) {
            throw new ClassCastException(object.getClass().getCanonicalName());
        }
        return (Call) object;
    }

    public static CallStatistic statistic(long id, Call call) {
        return new CallStatistic(id, Long.parseLong(call.from()), call.cost());
    }

    public static String keyStatistic(Call call) {
        return call.from() + ":" + DateUtils.toDay(call.start());
    }
}
