package name.gem.provider.commons.generators;

import name.gem.provider.commons.model.Call;

import java.util.Random;

public class CallGenerator {
    private static final Random random = new Random();
    private final int subscriber;
    private final int maxDuration;
    private final long castPerSecond;

    private CallGenerator(int subscriber, int maxDuration, long castPerSecond) {
        this.subscriber = subscriber;
        this.maxDuration = maxDuration;
        this.castPerSecond = castPerSecond;
    }

    public static CallGenerator build() {
        return new CallGenerator(1000, 3600, 2);
    }

    public Call next() {
        long duration = random.nextInt(maxDuration);
        return new Call(
                random.nextInt(subscriber),
                random.nextInt(subscriber),
                System.currentTimeMillis(), duration, duration * castPerSecond);
    }
}
