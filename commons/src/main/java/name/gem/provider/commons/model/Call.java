package name.gem.provider.commons.model;

public class Call implements Cloneable {
    private final String from;
    private final String to;
    private final long start;
    private final long duration;
    private final long cost;

    public Call(long from, long to, long start, long duration, long cost) {
        this.from = String.format("%08d", from);
        this.to = String.format("%08d", to);
        this.start = start;
        this.duration = duration;
        this.cost = cost;
    }

    public String from() {
        return from;
    }

    public String to() {
        return to;
    }

    public long start() {
        return start;
    }

    public long duration() {
        return duration;
    }

    public long cost() {
        return cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Call call = (Call) o;

        if (start != call.start) return false;
        if (duration != call.duration) return false;
        if (cost != call.cost) return false;
        if (from != null ? !from.equals(call.from) : call.from != null) return false;
        return to != null ? to.equals(call.to) : call.to == null;
    }

    @Override
    public int hashCode() {
        int result = from != null ? from.hashCode() : 0;
        result = 31 * result + (to != null ? to.hashCode() : 0);
        result = 31 * result + (int) (start ^ (start >>> 32));
        result = 31 * result + (int) (duration ^ (duration >>> 32));
        result = 31 * result + (int) (cost ^ (cost >>> 32));
        return result;
    }

    @Override
    public Call clone() {
        return new Call(Long.parseLong(from()), Long.parseLong(to()), start(), duration(), cost());
    }
}
