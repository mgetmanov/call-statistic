package name.gem.provider.commons.model;

public class CallStatistic {
    private final long id;
    private final long from;
    private final long cost;


    public CallStatistic(long id, long from, long cost) {
        this.id = id;
        this.from = from;
        this.cost = cost;
    }

    public long id() {
        return id;
    }

    public long from() {
        return from;
    }

    public long cost() {
        return cost;
    }
}
