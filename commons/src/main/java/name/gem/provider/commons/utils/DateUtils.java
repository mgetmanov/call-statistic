package name.gem.provider.commons.utils;

public class DateUtils {
    public static long toDay() {
        return toDay(System.currentTimeMillis());
    }

    public static long toDay(long ms) {
        return ms / 1000 / 3600 / 24;
    }
}
