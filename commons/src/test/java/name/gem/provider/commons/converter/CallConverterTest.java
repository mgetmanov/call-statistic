package name.gem.provider.commons.converter;

import name.gem.provider.commons.generators.CallGenerator;
import name.gem.provider.commons.model.Call;
import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;
import org.apache.ignite.binary.BinaryObject;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class CallConverterTest {
    private static Ignite ignite;

    @BeforeClass
    public static void initIgnite() {
        ignite = Ignition.start();
    }

    @Test
    public void testCall2BinaryObject() {
        Call call = CallGenerator.build().next();
        BinaryObject result = CallConverter.build(ignite.binary()).binaryObject(call);
        Assert.assertEquals(Call.class.getCanonicalName(), result.type().typeName());
    }

    @Test
    public void testBinaryObject2CallPositive() {
        Call object = CallGenerator.build().next();
        Call result = CallConverter.build(ignite.binary()).source(object);

        Assert.assertEquals(object.to(), result.to());
        Assert.assertEquals(object.from(), result.from());
        Assert.assertEquals(object.duration(), result.duration());
        Assert.assertEquals(object.cost(), result.cost());
        Assert.assertEquals(object.start(), result.start());
    }

    @Test
    public void testBinaryObject2CallNegative() {
        BinaryObject object = ignite.binary().builder(String.valueOf(System.currentTimeMillis())).build();
        try {
            CallConverter.build(ignite.binary()).source(object);
            Assert.fail();
        } catch (ClassCastException exc) {
            Assert.assertTrue(exc.getMessage(),
                    exc.getMessage().contains(object.getClass().getCanonicalName()));
        }
    }

    @AfterClass
    public static void closeIgnite() {
        ignite.close();
    }
}