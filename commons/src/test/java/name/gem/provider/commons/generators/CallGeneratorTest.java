package name.gem.provider.commons.generators;

import name.gem.provider.commons.model.Call;
import org.junit.Assert;
import org.junit.Test;

public class CallGeneratorTest {
    @Test
    public void next() throws Exception {
        CallGenerator generator = CallGenerator.build();
        for (int i = 0; i < 10_000; i++) {
            Call call = generator.next();
            Assert.assertEquals("from:" + call.from(), 8, call.from().length());
            Assert.assertEquals("to:" + call.to(), 8, call.to().length());
            Assert.assertTrue("duration:" + call.duration(), call.duration() >= 0);
        }
    }

}