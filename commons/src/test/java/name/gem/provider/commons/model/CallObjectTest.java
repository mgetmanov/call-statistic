package name.gem.provider.commons.model;

import name.gem.provider.commons.generators.CallGenerator;
import org.junit.Assert;
import org.junit.Test;

public class CallObjectTest {
    @Test
    public void testEquals() {
        Call call = CallGenerator.build().next();
        Assert.assertFalse(call.from().equals(call));
        Assert.assertTrue(call.clone().equals(call));
        Assert.assertFalse(call.equals(CallGenerator.build().next()));
    }

    @Test
    public void testHashCode() {
        Call call = CallGenerator.build().next();
        Assert.assertNotEquals(call.hashCode(), CallGenerator.build().next().hashCode());
        Assert.assertEquals(call.hashCode(), call.clone().hashCode());
    }
}