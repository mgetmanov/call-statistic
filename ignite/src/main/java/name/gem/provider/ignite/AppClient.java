package name.gem.provider.ignite;

import name.gem.provider.ignite.listeners.StatisticInitService;
import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.IgniteConfiguration;

public class AppClient {

    public static Ignite start() {
        IgniteConfiguration cfg = new IgniteConfiguration();
        cfg.setClientMode(true);
        cfg.setIgniteInstanceName("client_" + System.currentTimeMillis());
        cfg.setPeerClassLoadingEnabled(true);
        return
                activateClient(Ignition.start(cfg));
    }

    private static Ignite activateClient(Ignite server) {

        server.services().deployNodeSingleton(StatisticInitService.STAT_SERVICE, new StatisticInitService());
        return server;
    }
}
