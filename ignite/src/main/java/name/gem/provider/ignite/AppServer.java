package name.gem.provider.ignite;

import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;

public class AppServer {
    public static Ignite start() {
        return Ignition.start("https://raw.githubusercontent.com/MGetmanov/cfg/master/ignite_config.xml");
    }
}
