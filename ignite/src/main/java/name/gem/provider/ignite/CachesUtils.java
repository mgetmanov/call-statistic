package name.gem.provider.ignite;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.configuration.CacheConfiguration;

public class CachesUtils {
    private CachesUtils() {
    }

    public static <K, V> IgniteCache<K, V> getOrCreateCache(Ignite ignite, CacheConfiguration<K, V> cacheConfiguration) {
        if (ignite.cacheNames().stream().anyMatch(name -> name.equals(cacheConfiguration.getName()))) {
            return ignite.cache(cacheConfiguration.getName());
        }
        return ignite.getOrCreateCache(cacheConfiguration);
    }
}
