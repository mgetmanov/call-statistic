package name.gem.provider.ignite;

import name.gem.provider.commons.model.CallStatistic;
import org.apache.ignite.binary.BinaryObject;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.CacheMode;
import org.apache.ignite.configuration.CacheConfiguration;

import java.util.List;

public class IgniteConfig {
    private IgniteConfig() {
    }

    public final static CacheConfiguration<Long, BinaryObject> DETAILS = new CacheConfiguration<Long, BinaryObject>()
            .setCacheMode(CacheMode.PARTITIONED).setAtomicityMode(CacheAtomicityMode.ATOMIC).setName("Details");
    public final static CacheConfiguration<String, List<CallStatistic>> STATISTICS = new CacheConfiguration<String, List<CallStatistic>>()
            .setCacheMode(CacheMode.PARTITIONED).setAtomicityMode(CacheAtomicityMode.ATOMIC).setName("Statistics");
}
