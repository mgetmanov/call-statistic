package name.gem.provider.ignite.listeners;

import name.gem.provider.commons.converter.CallConverter;
import name.gem.provider.commons.model.Call;
import name.gem.provider.commons.model.CallStatistic;
import name.gem.provider.ignite.CachesUtils;
import name.gem.provider.ignite.IgniteConfig;
import name.gem.provider.ignite.processors.UpdateStatistic;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.binary.BinaryObject;
import org.apache.kafka.clients.producer.Producer;

import javax.cache.event.CacheEntryEvent;
import javax.cache.event.CacheEntryListenerException;
import javax.cache.event.CacheEntryUpdatedListener;
import java.util.List;

public class DetailsStatisticListener implements CacheEntryUpdatedListener<Long, BinaryObject> {
    private final UpdateStatistic updater;

    public DetailsStatisticListener(Producer<String, String> producer) {
        updater = new UpdateStatistic(producer);
    }

    @Override
    public void onUpdated(Iterable<CacheEntryEvent<? extends Long, ? extends BinaryObject>> events) throws CacheEntryListenerException {
        events.forEach(e -> processEntry(e));
    }

    private void processEntry(CacheEntryEvent<? extends Long, ? extends BinaryObject> e) {
        Object object = e.getValue();
        if (!(object instanceof Call)) {
            Ignition.ignite().log().warning(this.getClass().getCanonicalName() + " id:" + e.getKey()
                    + " type:" + object.getClass().getCanonicalName());
        }
        Call call = (Call) object;
        IgniteCache<String, List<CallStatistic>> statistic = CachesUtils.getOrCreateCache(Ignition.ignite(), IgniteConfig.STATISTICS);
        statistic.invoke(CallConverter.keyStatistic(call), updater, CallConverter.statistic(e.getKey(), call));
    }
}
