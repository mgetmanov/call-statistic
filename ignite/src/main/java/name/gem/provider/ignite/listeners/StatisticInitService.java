package name.gem.provider.ignite.listeners;

import name.gem.provider.commons.cfg.ConfigApp;
import name.gem.provider.ignite.CachesUtils;
import name.gem.provider.ignite.IgniteConfig;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.binary.BinaryObject;
import org.apache.ignite.cache.query.ContinuousQuery;
import org.apache.ignite.resources.IgniteInstanceResource;
import org.apache.ignite.services.Service;
import org.apache.ignite.services.ServiceContext;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;

import java.io.Serializable;

public class StatisticInitService implements Service, Serializable {
    private static final long serialVersionUID = 0L;

    public static final String STAT_SERVICE = "stat_service";

    @IgniteInstanceResource
    private Ignite ignite;
    private Producer<String, String> producer;

    @Override
    public void cancel(ServiceContext ctx) {
        ignite.log().info("Service was cancelled: " + ctx.name());
    }

    @Override
    public void init(ServiceContext ctx) throws Exception {
        producer = new KafkaProducer<>(ConfigApp.getPropertiesForKafka());

        IgniteCache<Long, BinaryObject> cache = CachesUtils.getOrCreateCache(ignite, IgniteConfig.DETAILS);
        ContinuousQuery<Long, BinaryObject> qry = new ContinuousQuery<>();
        qry.setLocalListener(new DetailsStatisticListener(producer));
        cache.query(qry);
        ignite.log().info("Service was initialized: " + ctx.name() + " on:" + ignite.name());
    }

    @Override
    public void execute(ServiceContext ctx) throws Exception {
        producer.close();
        ignite.log().info("Executing distributed service: " + ctx.name());
    }
}