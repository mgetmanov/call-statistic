package name.gem.provider.ignite.processors;

import name.gem.provider.commons.cfg.ConfigApp;
import name.gem.provider.commons.model.CallStatistic;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.CacheEntryProcessor;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import javax.cache.processor.EntryProcessorException;
import javax.cache.processor.MutableEntry;
import java.util.ArrayList;
import java.util.List;

public class UpdateStatistic implements CacheEntryProcessor<String, List<CallStatistic>, Object> {
    private Producer<String, String> producer;

    public UpdateStatistic(Producer<String, String> producer) {
        this.producer = producer;
    }

    @Override
    public Object process(MutableEntry<String, List<CallStatistic>> entry, Object... args) throws EntryProcessorException {
        if (!entry.exists()) {
            entry.setValue(new ArrayList<>());
        }
        CallStatistic call = (CallStatistic) args[0];
        List<CallStatistic> values = entry.getValue();
        long beforeSum = sum(values);
        if (beforeSum > ConfigApp.eventOfCost()) {
            return null;
        }
        values.add(call);
        entry.setValue(values);
        if (beforeSum + call.cost()> ConfigApp.eventOfCost()) {
            Ignition.ignite().log().info("new value:" + (beforeSum + call.cost()));
            producer.send(new ProducerRecord<String, String>("Alerts", Long.toString(call.from()), Long.toString(beforeSum + call.cost())));
        }
        return null;
    }

    private long sum(List<CallStatistic> values) {
        return values.stream().mapToLong(c -> c.cost()).sum();
    }
}