package name.gem.provider.ignite.repositories;

import name.gem.provider.ignite.IgniteConfig;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.binary.BinaryObject;
import org.apache.ignite.lang.IgniteFuture;

public class DetailsRep {
   private final Ignite ignite;

    private DetailsRep(Ignite ignite) {
        this.ignite = ignite;
        if (!ignite.cacheNames().contains(IgniteConfig.DETAILS.getName())) {
            ignite.createCache(IgniteConfig.DETAILS);
        }
    }

    public long save(long id, BinaryObject call) {
        IgniteCache<Long, BinaryObject> cache = ignite.cache(IgniteConfig.DETAILS.getName());
        cache.put(id, call);
        return id;
    }

    public IgniteFuture saveAsync(long id, BinaryObject call) {
        IgniteCache<Long, BinaryObject> cache = ignite.cache(IgniteConfig.DETAILS.getName());
        return cache.putAsync(id, call);
    }

    public Object get(long id) {
        return ignite.cache(IgniteConfig.DETAILS.getName()).get(id);
    }

    public static DetailsRep build(Ignite ignite) {
        return new DetailsRep(ignite);
    }
}
