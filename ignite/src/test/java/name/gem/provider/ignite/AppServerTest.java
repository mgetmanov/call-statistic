package name.gem.provider.ignite;

import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

public class AppServerTest {

    @Test
    public void testStart() throws Exception {
        Ignite result = AppServer.start();
        Assert.assertNotNull(result);
        Assert.assertTrue(result.active());
    }

    @After
    public void close() {
        Ignition.ignite().close();
        try {
            Ignition.ignite().active();
            Assert.fail("Must stopped");
        }catch (Exception e){
            //OK
        }
    }
}