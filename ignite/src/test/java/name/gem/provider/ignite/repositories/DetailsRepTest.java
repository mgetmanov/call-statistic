package name.gem.provider.ignite.repositories;

import name.gem.provider.commons.converter.CallConverter;
import name.gem.provider.commons.generators.CallGenerator;
import name.gem.provider.commons.model.Call;
import name.gem.provider.ignite.IgniteConfig;
import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;
import org.apache.ignite.binary.BinaryObject;
import org.junit.*;

public class DetailsRepTest {
    private static Ignite ignite;

    @BeforeClass
    public static void start() {
        ignite = Ignition.start();
    }

    @Test
    public void testSave() {
        Call source = CallGenerator.build().next();
        long key = System.currentTimeMillis();
        BinaryObject forTest = ignite.binary().toBinary(source);

        DetailsRep.build(ignite).save(key, forTest);
        Assert.assertEquals(source, ignite.cache(IgniteConfig.DETAILS.getName()).get(key));
    }

    @Test
    public void testGet() {
        Call call = CallGenerator.build().next();
        ignite.createCache(IgniteConfig.DETAILS).put(Long.parseLong(call.from()),
                CallConverter.build(ignite.binary()).binaryObject(call));

        DetailsRep repository = DetailsRep.build(ignite);
        Assert.assertEquals(call,
                repository.get(Long.parseLong(call.from())));
    }

    @After
    public void removeCache() {
        ignite.destroyCache(IgniteConfig.DETAILS.getName());
    }

    @AfterClass
    public static void close() {
        ignite.close();
    }
}