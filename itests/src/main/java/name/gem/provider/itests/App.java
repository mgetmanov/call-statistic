package name.gem.provider.itests;

import name.gem.provider.ignite.AppClient;
import org.apache.ignite.Ignite;

import java.util.Timer;

public class App {
    public static void main(String[] args) throws InterruptedException {
        try (
                Ignite client = AppClient.start()
        ) {
            Timer timer = new Timer();
            timer.schedule(new CallDataGenerator(client), 0, 1_000);
            Thread.sleep(10_000);
            timer.cancel();
        }
    }
}
