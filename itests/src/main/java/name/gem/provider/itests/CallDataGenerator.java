package name.gem.provider.itests;

import name.gem.provider.commons.cfg.ConfigApp;
import name.gem.provider.commons.converter.CallConverter;
import name.gem.provider.commons.generators.CallGenerator;
import name.gem.provider.ignite.IgniteConfig;
import name.gem.provider.ignite.repositories.DetailsRep;
import org.apache.ignite.Ignite;
import org.apache.ignite.lang.IgniteFuture;

import java.util.Collection;
import java.util.HashSet;
import java.util.Random;
import java.util.TimerTask;

public class CallDataGenerator extends TimerTask {
    private final Ignite client;


    CallDataGenerator(Ignite client) {
        this.client = client;
    }

    @Override
    public void run() {
        Collection<IgniteFuture> futures = iteration(client);
        while (!futures.stream().allMatch(IgniteFuture::isDone)) ;
        long after = client.cache(IgniteConfig.DETAILS.getName()).sizeLong();
        client.log().error("count: " + after);
    }

    private Collection<IgniteFuture> iteration(Ignite client) {
        CallGenerator generator = CallGenerator.build();
        CallConverter converter = CallConverter.build(client.binary());
        Random random = new Random();
        Collection<IgniteFuture> result = new HashSet<>();
        for (int i = 0; i < ConfigApp.callPerSeconds(); i++) {
            result.add(DetailsRep.build(client).saveAsync(random.nextLong(), converter.binaryObject(generator.next())));
        }
        return result;
    }
}