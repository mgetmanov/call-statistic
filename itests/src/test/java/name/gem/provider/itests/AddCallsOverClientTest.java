package name.gem.provider.itests;

import junit.framework.TestCase;
import name.gem.provider.ignite.AppClient;
import name.gem.provider.ignite.AppServer;
import name.gem.provider.ignite.CachesUtils;
import name.gem.provider.ignite.IgniteConfig;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.binary.BinaryObject;
import org.junit.Assert;

import java.util.Timer;

public class AddCallsOverClientTest extends TestCase {
    private static final Integer CALL_PER_SECOND = 100;
    private static final Integer ITERATIONS = 10;

    public void test() throws InterruptedException {
        try (
                Ignite server = AppServer.start();
                Ignite client = AppClient.start()
        ) {
            IgniteCache<Long, BinaryObject> details = CachesUtils.getOrCreateCache(client, IgniteConfig.DETAILS);
            Timer timer = new Timer();
            timer.schedule(new CallDataGenerator(client), 0, 1_000);
            Thread.sleep(ITERATIONS * 1_000);
            timer.cancel();
            Assert.assertEquals(ITERATIONS * CALL_PER_SECOND, details.size());
        }
    }
}
