package name.gem.provider.itests;

import junit.framework.TestCase;
import name.gem.provider.commons.converter.CallConverter;
import name.gem.provider.commons.model.Call;
import name.gem.provider.commons.model.CallStatistic;
import name.gem.provider.ignite.AppClient;
import name.gem.provider.ignite.AppServer;
import name.gem.provider.ignite.IgniteConfig;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.binary.BinaryObject;
import org.junit.Assert;

import java.util.List;

public class AddStatisticEntryTest extends TestCase {
    public void test() throws InterruptedException {
        try (
                Ignite server = AppServer.start();
                Ignite client = AppClient.start();
                Ignite client2 = AppClient.start();
        ) {
            long putResult = 0;
            putResult = checkInsert(server, putResult);
            putResult = checkInsert(client, putResult);
            checkInsert(client2, putResult);
        }
    }

    private long checkInsert(Ignite ignite, long before) throws InterruptedException {
        Call call = new Call(1, 2, 3, 4, 5);
        IgniteCache<String, List<CallStatistic>> statistics = ignite.cache(IgniteConfig.STATISTICS.getName());
        if (statistics != null)
            Assert.assertEquals(before, statistics.get(CallConverter.keyStatistic(call)).stream().mapToLong(c -> c.cost()).sum());

        IgniteCache<Long, BinaryObject> details = ignite.cache(IgniteConfig.DETAILS.getName());

        details.put(1L, CallConverter.build(ignite.binary()).binaryObject(call.clone()));
        details.put(2L, CallConverter.build(ignite.binary()).binaryObject(call.clone()));
        details.put(3L, CallConverter.build(ignite.binary()).binaryObject(call.clone()));
        details.put(4L, CallConverter.build(ignite.binary()).binaryObject(call.clone()));
        Thread.sleep(5_000L);
        statistics = ignite.cache(IgniteConfig.STATISTICS.getName());

        long result = statistics.get(CallConverter.keyStatistic(call)).stream().mapToLong(c -> c.cost()).sum();
        Assert.assertEquals(before + 4 * call.cost(), result);
        return result;
    }
}
