package name.gem.provider.itests;

import junit.framework.TestCase;
import name.gem.provider.ignite.AppClient;
import name.gem.provider.ignite.AppServer;
import org.apache.ignite.Ignite;
import org.apache.ignite.cluster.ClusterNode;
import org.junit.Assert;

import java.util.Collection;
import java.util.Iterator;

public class ClientServerGridTest extends TestCase {
    public void test() {
        try (
                Ignite server = AppServer.start();
                Ignite client = AppClient.start()
        ) {
            Collection<ClusterNode> serverTopo = server.cluster().topology(server.cluster().topologyVersion());
            Collection<ClusterNode> clientTopo = client.cluster().topology(server.cluster().topologyVersion());
            Iterator<ClusterNode> serverIterator = serverTopo.iterator();
            Iterator<ClusterNode> clientIterator = clientTopo.iterator();
            Assert.assertEquals(serverTopo.size(), 2);
            Assert.assertEquals(clientTopo.size(), 2);
            Assert.assertNotEquals(serverIterator.next().isClient(), serverIterator.next().isClient());
            Assert.assertNotEquals(clientIterator.next().isClient(), clientIterator.next().isClient());
        }
    }
}
